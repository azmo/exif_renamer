Just a very basic python script to rename JPEG photos using their embedded EXIF data.

    Usage:  ./exif_renamer.py <base> <image>
            ./exif_renamer.py "Luke I'm your father" image3221.jpg

###Dependencies:

- [pyexiv2](http://tilloy.net/dev/pyexiv2/)

###What this does:

- opens the file you throw at it
- reads its metadata
- renames (actually moves) to something like this:

        <base>_<yearmonthday>_<hourminsec>_<xdimension>x<ydimension>.jpg
        luke_im_your_father_20120211_171618_2560x1920.jpg

- any spaces in base will be converted to underscores

###Benefits:

- consistent naming of your files
- sane sorting by date/time

###License:

This is so simple, do w/e you like with it.

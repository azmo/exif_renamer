#!/usr/bin/env python

from __future__ import print_function
import pyexiv2
import sys
import shutil

def renameImage(image=None,metadata=None,base=None):
    x_dimension = metadata['Exif.Photo.PixelXDimension'].value
    y_dimension = metadata['Exif.Photo.PixelYDimension'].value
    datetime    = metadata['Exif.Photo.DateTimeOriginal'].value
    datetime    = datetime.strftime('%Y%m%d_%H%M%S')

    base        = base.replace(' ','_')
    destination = "{0}_{1}_{2}x{3}.jpg".format(base.lower(), datetime,x_dimension,y_dimension)

    try:
        shutil.move(image, destination)
    except shutil.Error as e:
        print("Could not move {0} to {1}: {2}".format(image,destination, str(e)),file=sys.stderr)
        sys.exit(3)

if __name__ == '__main__':
    try:
        base  = sys.argv[1]
        image = sys.argv[2]
    except:
        print("Usage: {0} <basedescription> <image>".format(sys.argv[0]))
        sys.exit(1)

    try:
        metadata = pyexiv2.ImageMetadata(image)
        metadata.read()
    except BaseException as e:
        print("There was an error processing {0}. {1}".format(image,str(e)),file=sys.stderr)
        sys.exit(2)
    else:
        renameImage(image, metadata, base)

    sys.exit(0)
